<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <style>
    .templateux-overlap .block-icon-1 {

        margin-top: 0px;
        position: relative;
    }

    @media only screen and (max-width: 767px) {
        .box_detail {
            margin-bottom: 10px !important;
        }
    }

    .align-items-lg-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/b5.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">IVA 債務重組</h1>
                        <p class="lead mb-5 text-white" data-aos="fade-up" data-aos-delay="100">債務重組（IVA）又稱為個人自願安排（Individual Voluntary Arrangement)）。申請IVA的最大原因，是欠債人士無法依照原有債務規定還債，因此希望債主體恤自己情況，跟債主磋商一個新的還款方案。</p>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section">
            <style>
            .blue_bk_w {
                color: #fff;
                background: #1472eb;
            }

            .blue_bk_b {
                color: #000;
                background: #1472eb;
            }

            .b_l {
                border-bottom: solid 1px #e6e6e6;
            }
            </style>
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="">
                        <h2 class="mb-5">IVA個人自願安排(債務重組)</h2>
                        <div class=" ">
                            <div class="">
                                <div class="elementor-widget-container">
                                    <p>根據破產條例第6章第20條，個人自願安排 (Individual Voluntary Arrangement，IVA 債務重組) 是面臨破產之人仕的另一個選擇。 債務人須向其債權人提出償還債項建議，經過法庭聆訊及頒佈臨時命令，並獲債權人通過其建議書，確保債務人的資產可獲妥善分派給債權人。</p>
                                    <h4 class="mb-5">IVA個人自願安排(債務重組)</h4>
                                    <ul>
                                        <li>有較穩定收入而欠債超過月薪十多倍以上的債務人</li>
                                        <li>敏感職業而不能申請破產的債務人</li>
                                    </ul>
                                    <h4 class="mb-5">IVA可重組以下之債務:</h4>
                                    <ul>
                                        <li>卡數｜銀行私人貸款｜財務公司貸款｜”一筆清”高息貸款｜儲蓄互助社貸款｜學生資助貸款｜稅項</li>
                                    </ul>
                                    <h4 class="mb-5">申請程序及所需時間 (由提出申請日起計)</h4>
                                    <table>
                                        <tr class="blue_bk_w">
                                            <td>申請程序</td>
                                            <td>預計時間</td>
                                        </tr>
                                        <tr class="blue_bk_b">
                                            <td colspan="2">第1階段</td>
                                        </tr>
                                        <tr class="b_l">
                                            <td colspan="2">債務人帶備有關債項及入息等資料進行IVA債務重組諮詢及分析，得到我們接納辦理後， 由代名人擬定還款建議書(當然，我們只接納有把握之申請，因為有部份的欠債情況實在不適合進行IVA債務重組，我們絕不會浪費閣下金錢及時間)我們亦會以豐富經驗平衡債權人及債務人利益，去編寫債務重組建議書，當中除了建議債項的每月還款額、期數、利率外，亦列明了申請人的生活所需開支；申請人的資產如自住物業、私家車、保險、退休金等亦會清楚交代。 - 第 1 日</td>
                                        </tr>
                                        <tr class="">
                                            <td>進行宣誓，由代表律師將臨時命令申請書呈送法庭存檔</td>
                                            <td>約 2 星期內</td>
                                        </tr>
                                        <tr class="blue_bk_b">
                                            <td colspan="2">第2階段</td>
                                        </tr>
                                        <tr class="b_l">
                                            <td>法庭聆訊，頒佈IVA臨時命令</td>
                                            <td>約 3 個月</td>
                                        </tr>
                                        <tr class="">
                                            <td>刊登債權人會議通告(中英文報紙各一份)</td>
                                            <td>約 3 個半月</td>
                                        </tr>
                                        <tr class="blue_bk_b">
                                            <td colspan="2">第2階段</td>
                                        </tr>
                                        <tr class="b_l">
                                            <td colspan="2">獲出席和投票的債權人或其代表以大多數通過決議（即佔出席及投票債權人所持債款總值75%以上），自願安排實行，每月依時繳交還款，再由代名人安排還款予債權人。</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- .owl-carousel -->
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>