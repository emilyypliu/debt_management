<?php
    $url_info = parse_url($_SERVER['REQUEST_URI'], -1);
    
    $case_index = $url_info['query'];
    if(!$case_index){
        header("Location: " . getenv('DOMAIN'));
        die();
    }

	$case_json = json_decode(file_get_contents("case.json"), true);
    if(!isset($case_json[$case_index])){
        header("Location: " . getenv('DOMAIN'));
        die();
    }
    
    $case = $case_json[$case_index];
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <style>
        .cases .templateux-cover,.cases .templateux-cover .row {
          min-height: 400px;
        }

        .cases .templateux-cover {
         background-position: top;
        }

        @media (min-width: 768px){
            .cases .templateux-cover .heading {
                font-size: 24px;
            }
            .templateux-overlap .block-icon-1 {
                margin-top: 0px;
                height: 100%;
                top: -200px;
                position: relative;
            }
        }
    </style>
    <div class="cases js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/b2.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">債務舒緩案例</h1>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div id="blog" class="templateux-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="mb-3"><?php echo $case['title']; ?></h2>
                        <div class="elementor-widget-container">
                            <?php echo $case['content']; ?>
                        </div>
                    </div> <!-- .col-md-8 -->
                    <div class="col-md-4 sidebar">
                        <div class="sidebar-box">
                            <img src="<?php echo $case['image']; ?>" alt="Image placeholder" class="img-fluid mb-4 rounded">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>