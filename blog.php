<?php 
    $case_json = json_decode(file_get_contents("case.json"), true);
    if(!$case_json){
        header("Location: " . getenv('DOMAIN'));
        die();
    }
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/b3.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">成功案例參考</h1>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section">
            <div class="container">
                <div class="row">
<?php foreach($case_json as $case_index => $case): ?>
    <?php if(!$case)continue; ?>
                    <div class="col-md-6 col-lg-4 mb-4">
                        <a href="case?<?php echo $case_index; ?>" class="block-thumbnail-1 one-whole show-text height-sm" style="background-image: url('<?php echo $case['image']; ?>'); " data-aos="fade" data-aos-delay="300">
                            <div class="block-thumbnail-content">
                                <h2><?php echo $case['service']; ?> <?php echo $case['title']; ?></h2>
                                <span class="post-meta"><?php echo $case['post_date']; ?></span>
                            </div>
                        </a>
                    </div>
<?php endforeach; ?>
                </div>
            </div> <!-- .row -->
        </div>
    </div> <!-- .templateux-section -->
    <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>