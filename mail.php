<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require __DIR__ . '/vendor/autoload.php';

try {
	session_start();
	///////////////////////////////////////////////////////
	// CHECK POST METHOD //////////////////////////////////
	///////////////////////////////////////////////////////
	if(
		$_SERVER['REQUEST_METHOD'] !== 'POST'
		||
		!$_POST
	){
		throw new Exception ('請填寫內容');
	}

	///////////////////////////////////////////////////////
	// CHECK FIELD LIST ///////////////////////////////////
	///////////////////////////////////////////////////////
	$field_list = [
		"name" => "名稱",
		"email" => "電郵",
		"phone" => "電話號碼",
		"message" => "查諮內容",
	];
	$error_fields = [];
	foreach($field_list as $field => $field_name){
		if(!$_POST[$field]){
			$error_fields[] = $field_name;
		}
		$_SESSION['contact_form'][$field] = $_POST[$field];
	}
	if(count($error_fields) > 0){
		throw new Exception ("請輸入" . implode(", ", $error_fields));
	}

	///////////////////////////////////////////////////////
	// GENERATE EMAIL BODY ////////////////////////////////
	///////////////////////////////////////////////////////
	$body = "";
	$alt_body = "";
	foreach($field_list as $field => $field_name){
		$temp_str = $field_name . ": " . $_SESSION['contact_form'][$field];
		$body .= $temp_str . "<br>";
		$alt_body .= $temp_str . "; \n";
	}
	
	///////////////////////////////////////////////////////
	// SENDING EMAIL //////////////////////////////////////
	///////////////////////////////////////////////////////
	//Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER; //For Debug Only
    $mail->isSMTP();
    $mail->Host       = getenv('SMTP_HOST');
    $mail->SMTPAuth   = true;
    $mail->Username   = getenv('SMTP_USER');
    $mail->Password   = getenv('SMTP_PASS');
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port       = getenv('SMTP_PORT');

    //Recipients
    $mail->setFrom(getenv('SMTP_FROM'), 'HKPDRS 網站');
    $mail->addAddress(getenv('SMTP_TO'), 'HKPDRS');

    //Content
    $mail->isHTML(true);
    $mail->CharSet="UTF-8";
    $mail->Subject = '網站查詢';
    $mail->Body    = $body;
    $mail->AltBody = $alt_body;

    $mail->send();
    
    $_SESSION['contact_form']['alert_message'] = '<strong>郵件已經成功發出</strong>， 我們將會與閣下聯絡';
    $_SESSION['contact_form']['success'] = true;
} catch (Exception $e) {
	$_SESSION['contact_form']['alert_message'] = "<strong>郵件沒有成功發出</strong>: " . $e->getMessage();
	$_SESSION['contact_form']['success'] = false;
}

header("Location: " . getenv('DOMAIN') . "#contact");
die();