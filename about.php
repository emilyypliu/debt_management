<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>
    <style>
    .templateux-overlap .block-icon-1 {

        margin-top: 0px;
        position: relative;
    }

    @media only screen and (max-width: 767px) {
        .box_detail {
            margin-bottom: 10px !important;
        }
    }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/b2.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">關於我們</h1>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section pt-0 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 templateux-overlap">
                        <p>本公司一直秉持着「助人為本」的精神，致力為客戶提供最「專業、可靠、靈活」的財務策劃，本公司對於財務策劃之獨特專業意見，絕對可以幫助客戶輕鬆解決各種財務或債務上之困難和問題。</p>
                        <h6>專業團隊，貼心服務</h6>
                        <p style="font-weight: bolder;">1) 為客戶提供評估方案，以作參考</p>
                        <p>不論申請與否,本公司承諾所有查詢、物業估價、面談、專業分析均不設收費。</p>
                        <p style="font-weight: bolder;">2) 為客戶提供更多、更新和更準確的貸款資訊</p>
                        <p>龐大的金融網絡,透過本公司,您可同時掌握多家銀行及財務機構的按揭計劃及最新優惠詳情。無論是物業新按、轉按或加按，破產、贖樓、債務重組，本公司都能提供給客戶卓越的服務。</p>
                        <p style="font-weight: bolder;">3) 由申請至批核按揭，全程跟進，您毋須奔波勞碌，費時傷神</p>
                        <p>本公司提供一站式服務。由接觸客戶開始，全程由專業的財務策劃顧問跟進，以客為本，提供一對一服務。從分析客戶資料、預約評估、直至成功，辦理過程的每一個步驟由財務策劃顧問替客戶安排、辦理。為客戶節省了處理各種文件的時間，同時也避免了在辦理過程中的諸多不便。</p>
                        <p style="font-weight: bolder;">4) 不同的計劃，適合不同的客戶。我們會針對您的需要，給您度身訂造最合適的計劃</p>
                        <p>本公司掌握最新的政策、優點和缺點。如果客戶自己辦理的話，了解的資訊只會是單方面的。相反,我們會在完全掌握客戶的基本資料和需要後，做一個詳盡的分析，憑著豐富的理財經驗，制訂出最合適的方案供客戶選擇。</p>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <div class="templateux-section">
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="col-lg-7">
                        <h2 class="mb-5">樓按加按</h2>
                        <div class="owl-carousel wide-slider">
                            <div class="item">
                                <img src="images/hm_1.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="images/hm_3.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="images/hm_2.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
                            </div>
                        </div> <!-- .owl-carousel -->
                    </div>
                    <div class="col-lg-5 pl-lg-5">
                        <h2 class="mb-5">Why Us?</h2>
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="mb-0 rounded mb-2">
                                    <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        加按/轉按/二按清債計劃</a>
                                </h2>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>適合擁有正資產物業的人士 (半邊業權也可)</p>
                                        <p>最高可借85%樓價</p>
                                        <p>非統一樓按及其他債務, 定額還款, 以減低利息開支</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="mb-0 rounded mb-2">
                                    <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        加按/轉按/二按清債申請程序
                                    </a>
                                </h2>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>提供現時物業資料、樓按餘額、還款紀錄等</p>
                                        <p>提供近三個月入息記錄</p>
                                        <p>提供私人貸款 / 信用咭等欠債資料</p>
                                        <p>本公司會為客戶進行估價及選取最優惠最合適的按揭計劃</p>
                                        <p>本公司安排客戶到有關銀行或財務機構簽署有關之按揭文件</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="mb-0 rounded mb-2">
                                    <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        加按/轉按清債參考個案
                                    </a>
                                </h2>
                                <style>
                                .money_table td {
                                    border: solid 1px;
                                }
                                </style>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <table class="money_table" style="border: solid 1px;">
                                            <tr>
                                                <td></td>
                                                <td>卡數/貸款</td>
                                                <td>樓按</td>
                                                <td>加按清卡數</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>結餘</td>
                                                <td>HK$ 500,000</td>
                                                <td>HK$ 1,200,000</td>
                                                <td>HK$ 1,700,000</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>每月還款</td>
                                                <td>HK$25,000</td>
                                                <td>HK$ 7,800</td>
                                                <td>HK$ 13,400</td>
                                                <td>還款額大減60%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <div class="templateux-section pt-0 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 templateux-overlap">
                        <div class="row">
                            <div class="col-md-3 box_detail" data-aos="fade-up" data-aos-delay="800">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/004-gear.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">BANKRUPT 破產</h3>
                                        <p>當欠債人不能償還其債務，又或因失業 , 收入低 , 家庭生活開支等等原因...未能處理欠債，便可選擇申請破產來終止 銀行/財務公司等 滋擾性追數行動。破產是一種對個人及家庭生活的保障。這個保障是透過法律程序，保障欠債人不會因欠債導致個人及家庭成員的生活受還債壓力而有所影響。</p>
                                        <p><a href="bankrupt.html">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                            <div class="col-md-3 box_detail" data-aos="fade-up" data-aos-delay="600">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/001-consultation.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">IVA 債務重組</h3>
                                        <p>債務重組（IVA）又稱為個人自願安排（Individual Voluntary Arrangement)）。申請IVA的最大原因，是欠債人士無法依照原有債務規定還債，因此希望債主體恤自己情況，跟債主磋商一個新的還款方案。</p>
                                        <p><a href="iva.html">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                            <div class="col-md-3 box_detail" data-aos="fade-up" data-aos-delay="700">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/002-discussion.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">DRP 債務舒緩</h3>
                                        <p>債務舒緩（DRP），是IVA的簡化版，毋須經法庭申請重整債務。欠債人可自行跟債主商量，達成新的還款方案，包括降低利息或重訂還款年期；也可以尋找律師擬定還款建議書，詳列欠債人的資產及負債狀況，跟債主磋商。</p>
                                        <p><a href="drp.html">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                            <div class="col-md-3 box_detail" data-aos="fade-up" data-aos-delay="800">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/003-turnover.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">IDRP 綜合債務舒緩</h3>
                                        <p>透過與欠債人的最大債權人商談還款方案的服務，只要最大債權人同意新提出的還款方案，欠債人所有的債務將會被統一歸納至最大債權人之下，並且按照新的還款建議書的還款方法，以定息定額之形式每月償還債款予最大債權人。</p>
                                        <p><a href="idrp.html">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>