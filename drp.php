<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <style>
    .templateux-overlap .block-icon-1 {

        margin-top: 0px;
        position: relative;
    }

    @media only screen and (max-width: 767px) {
        .box_detail {
            margin-bottom: 10px !important;
        }
    }

    .align-items-lg-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/b4.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">DRP 債務舒緩</h1>
                        <p class="lead mb-5 text-white" data-aos="fade-up" data-aos-delay="100">債務舒緩（DRP），是IVA的簡化版，毋須經法庭申請重整債務。欠債人可自行跟債主商量，達成新的還款方案，包括降低利息或重訂還款年期；也可以尋找律師擬定還款建議書，詳列欠債人的資產及負債狀況，跟債主磋商。</p>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section">
            <style>
            .blue_bk_w {
                color: #fff;
                background: #1472eb;
            }

            .blue_bk_b {
                color: #000;
                background: #1472eb;
            }

            .b_l {
                border-bottom: solid 1px #e6e6e6;
            }
            </style>
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="">
                        <h2 class="mb-5">DRP債務舒緩計劃</h2>
                        <div class=" ">
                            <div class="">
                                <div class="elementor-widget-container">
                                    <p>債務舒緩計劃 (DRP) 是債務重組其中之一種方法，可以說是IVA的簡化版，其中最大之分別在於無需透過法律程序，更可與個別的債權人直接商談還款方案，使欠債人避免破產而能在新的還款方法下，重過正常人之生活，及在指定之年期間還清債項。 其實對部份欠債人來說，用DRP債務舒緩計劃來重組債務，甚至較IVA更為適合。例如欠債人的債權人(包括銀行、財務公司等)數目比較少，而該些債權人也願意接納債務舒緩計劃 (DRP)，這樣，欠債人就可以選擇採用DRP這個申請費較低，申請時間較短的計劃。又例如一些從事金融業或紀律部隊的人士，由於僱主會獲通知其申請IVA，可能會導致失去工作, 所以他們一般都會選擇DRP。 首先欠債人須提供所有個人之欠債資料紀錄，經由專業人士分析。 經過分析後，DRP不像IVA需要委任代名人替欠債人擬定還款報告，另外，亦不需要聘請律師向法庭申請臨時命令，亦不需要上庭開債權人會議。 欠債人只需經過本公司替其擬定一份說明欠債人之還款能力，還款額、利率、期數及還款方式之建議上，之後，本公司會分別向個別之債權人獨立提交，並直接與其商討一個能符合雙方利益之協議。整個過程大約需一個半月至兩個月時間。 協議一經諾實，欠債人會與債權人簽署新的還款協議書並履行新的還款方案。直接還款給債權人。 透過DRP與債權人達成新的還款方案，使欠債人可以大大減少利息的支付，並且可以在可見的數年間償還所有債項，使欠債人減輕生活負擔，維持在合理的生活水平。 適合敏感之行業，如銀行、保險、金融及紀律部隊等 由於DRP不需要經過律師，會計師以及不須上庭，另外，DRP成功後，不須透過代名人安排其還款，所以DRP可以省卻很多不必要的行政費，如專業人士之費用及堂費等，因此費用較IVA優惠。 DRP的還款方案是個別獨立提交債權人，不似IVA需於庭上作出表決，而表決結果需多於75%的負債比例方為成功，故此，DRP可避免庭上之骨牌效應，風險較低，成功率高。 欠債人可以避免破產，也不會有破產紀錄，更重要的是不會影響現有工作及聲譽。
                                    </p>
                                    <h4 class="mb-5">申請資格</h4>
                                    <h5 class="mb-5">DRP適合：</h5>
                                    <ul>
                                        <li>債權人數目一個或以上便可申請。</li>
                                        <li>欠債銀碼低於十萬者亦可辦理。(但必須超過月薪十倍)</li>
                                        <li>非常適合敏感行業。(如銀行職員, 紀律部隊等)</li>
                                    </ul>
                                    <h5 class="mb-5">DRP好處：</h5>
                                    <ul>
                                        <li>可選擇重組部份債務。</li>
                                        <li>可保留部份信用卡或信貸戶口。</li>
                                    </ul>
                                </div>
                            </div>
                        </div> <!-- .owl-carousel -->
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>