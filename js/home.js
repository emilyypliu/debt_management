var1 = parseInt($(".bank_debt").val());
var2 = parseInt($(".company_debt").val());
var slider = document.getElementById("progress");
var output = document.getElementById("demo");
var output2 = document.getElementById("demo2");
var slider2 = document.getElementById("progress2");
var bank_de = document.getElementById("bank_de");
var company_de = document.getElementById("company_de");
var interest = document.getElementById("interest");
// var bank_de = slider;
output.innerHTML = slider.value; // Display the default slider value

bank_de.innerHTML = Math.floor((var1 / slider.value) * 100) / 100;
company_de.innerHTML = Math.floor((var2 / slider2.value) * 100) / 100;
// var output2 = document.getElementById("demo2");
output2.innerHTML = slider2.value; // Display the default slider value
// Update the current slider value (each time you drag the slider handle)
if (output.innerHTML = slider.value) {
    slider.oninput = function() {
        output.innerHTML = this.value;
        bank_de.innerHTML = Math.floor((var1 / this.value) * 100) / 100;
        interest.innerHTML = var1 + (var2 / 2);
    }
}
if (output2.innerHTML = slider2.value) {
    slider2.oninput = function() {
        output2.innerHTML = this.value;
        company_de.innerHTML = Math.floor((var2 / this.value) * 100) / 100;
        interest.innerHTML = var2 + (var2 / 2);
    }
}

$(document).ready(function() {
    var1 = parseInt($(".bank_debt").val());
    var2 = parseInt($(".company_debt").val());
    sum = (var1 + var2);
    $(".bank_debt_num").html(var1);
    $(".company_debt_num").html(var2);
    $(".answer").html(sum);

    $(".bank_debt").keyup(function() {
        var1s = $(".bank_debt").val();
        if (var1s.match(/^\d+$/)) {
            $('.messageError').html('');
            sums();
        } else {
            $('.messageError').html('sorry number only for the first value');
        }
    });

    $(".company_debt").keyup(function() {
        var2s = $(".company_debt").val();
        if (var2s.match(/^\d+$/)) {
            $('.messageError').html('');
            sums();
        } else {
            $('.messageError').html('sorry number only for the second value');
        }
    });


    $(".add").click(function() {
        $('.operator').html('+');
        sums();
    });
    $(".minus").click(function() {
        $('.operator').html('-');
        sums();
    });
    $(".times").click(function() {
        $('.operator').html('x');
        sums();
    });
    $(".divide").click(function() {
        $('.operator').html('/');
        sums();
    });

    function sums() {
        console.log('testtest');
        var1 = parseInt($(".bank_debt").val());
        var2 = parseInt($(".company_debt").val());
        operator = $(".operator").html();
        sum = (var1 + var2);
        switch (operator) {
            case '+':
                sum = (var1 + var2);
                break;
            case '-':
                sum = (var1 - var2);
                break;
            case 'x':
                sum = (var1 * var2);
                break;
            case '/':
                sum = (var1 / var2);
                break;
            default:
                sum = (var1 + var2);
        }
        $(".answer").html(sum);
        $(".bank_debt_num").html(var1);
        $(".company_debt_num").html(var2);
        console.log(var1);
        console.log(output);
        bank_de.innerHTML = Math.floor((var1 / slider.value) * 100) / 100;
        company_de.innerHTML = Math.floor((var2 / slider2.value) * 100) / 100;
        interest.innerHTML = var1 + (var2 / 2);

        $(".answer").each(function(c, obj) {
            $(obj).text(addCommas(parseFloat($(obj).text()).toFixed(2)));
        });
        $(".bank_debt_num").each(function(c, obj) {
            $(obj).text(addCommas(parseFloat($(obj).text()).toFixed(2)));
        });
        $(".company_debt_num").each(function(c, obj) {
            $(obj).text(addCommas(parseFloat($(obj).text()).toFixed(2)));
        });

    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
});