<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <style>
    .templateux-overlap .block-icon-1 {

        margin-top: 0px;
        position: relative;
    }

    @media only screen and (max-width: 767px) {
        .box_detail {
            margin-bottom: 10px !important;
        }
    }

    .align-items-lg-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/g3.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">BANKRUPT 破產</h1>
                        <p class="lead mb-5 text-white" data-aos="fade-up" data-aos-delay="100"></p>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section">
            <style>
            .blue_bk_w {
                color: #fff;
                background: #1472eb;
            }

            .blue_bk_b {
                color: #000;
                background: #1472eb;
            }

            .b_l {
                border-bottom: solid 1px #e6e6e6;
            }
            </style>
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="">
                        <h2 class="mb-5">BANKRUPT 破產</h2>
                        <div class=" ">
                            <div class="">
                                <div class="elementor-widget-container">
                                    <p>當欠債人不能償還其債務，又或因失業 , 收入低 , 家庭生活開支等等原因...未能處理欠債，便可選擇申請破產來終止 銀行/財務公司等 滋擾性追數行動。破產是一種對個人及家庭生活的保障。這個保障是透過法律程序，保障欠債人不會因欠債導致個人及家庭成員的生活受還債壓力而有所影響。
                                    </p>
                                </div>
                            </div>
                        </div> <!-- .owl-carousel -->
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>