<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <style>
    .templateux-overlap .block-icon-1 {

        margin-top: 0px;
        position: relative;
    }

    @media only screen and (max-width: 767px) {
        .box_detail {
            margin-bottom: 10px !important;
        }
    }

    .align-items-lg-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }

    .blue_bk_w {
        color: #fff;
        background: #1472eb;
    }

    .blue_bk_b {
        color: #000;
        background: #1472eb;
    }

    .b_l {
        border-bottom: solid 1px #e6e6e6;
    }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/b6.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">IDRP綜合債務舒緩計劃</h1>
                        <p class="lead mb-5 text-white" data-aos="fade-up" data-aos-delay="100">透過與欠債人的最大債權人商談還款方案的服務，只要最大債權人同意新提出的還款方案，欠債人所有的債務將會被統一歸納至最大債權人之下，並且按照新的還款建議書的還款方法，以定息定額之形式每月償還債款予最大債權人。</p>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section">
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="">
                        <h2 class="mb-5">IDRP綜合債務舒緩計劃
                        </h2>
                        <div class=" ">
                            <div class="">
                                <div class="elementor-widget-container">
                                    <p>IDRP是透過與欠債人的最大債權人商談還款方法的綜合債務舒緩計劃。在整個過程中，只要這最大債權人願意接受欠債人的還款方案，而其他債權人亦同意此方案, 最大債權人便會統一欠債人之所有欠款，從此，欠債人只需要跟據議案上之還款方法，以定息定額之形式，每月攤還所有款項給這最大債權人。IDRP之好處在於只須與最大債權人洽談，而無須與其他債權人個別談判，手續較DRP更為簡單。</p>
                                </div>
                            </div>
                        </div> <!-- .owl-carousel -->
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>