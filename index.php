<?php 
    session_start();

    $contact_form_alert = false;
    if(isset($_SESSION['contact_form'])){
        $contact_form = $_SESSION['contact_form'];
    }
    
    session_destroy();
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>
    <style>
        .align-items-lg-center {
          -webkit-box-align: center!important;
          -ms-flex-align: center!important;
          align-items: center!important;
        }
        .animsition, .animsition-overlay {
          -webkit-animation-fill-mode: none !important;
        }
        .templateux-cover {
          position: initial; 
        }
        .templateux-cover:before {
          max-height: 800px;
        }
        @media screen and (max-width: 766px){
          .templateux-cover:before {
            max-height: 902px;
          }
        }
        .table_out {
          display: table;
          width: 100%;
        }
        .table_in {
          display: table-cell;
          padding: 16px;
        }
        @media only screen and (max-width: 600px) {
          .table_in { 
            display: block;
            width: 100%;
          }
        }
        .money_table td{
          border: solid 1px;
        }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/1.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 ml-3">
                        <p class="lead  text-white" data-aos="fade-up" data-aos-delay="100">為你立即提供詳盡協助及分析</p>
                        <h1 class="heading mb-3 text-white" data-aos="fade-up"> <strong>解決各種債務問題</strong></h1>
                        <a href="about" style="z-index: 99999; background-color: rgba(2, 117, 216, 0.4); color: rgba(41, 43, 44, 0.9);" class="btn btn-primary px-3 py-0 mr-3" data-aos="fade-up" data-aos-delay="200">
                            <span style="font-size:28px; margin: 0;">關於我們</span>
                        </a>
                        <a href="/#contact" style="z-index: 99999; background-color: rgba(2, 117, 216, 0.4); color: rgba(41, 43, 44, 0.9);" class="btn btn-primary px-3 py-0 mr-3" data-aos="fade-up" data-aos-delay="200">
                            <span style="font-size:28px; margin: 0;">聯絡我們</span>
                        </a>
                        <a href="https://api.whatsapp.com/send?phone=85251731101" style="width: 270px;z-index: 99999;display: block;padding: 14px 0;" class="btn" data-aos="fade-up" data-aos-delay="200"><img src="images/whatsapp_v2.png" alt="Image" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section pt-0 pb-0">
            <div class="container">
                <div class="row table_out ">
                    <div class="col-md-12 templateux-overlap">
                        <div class="row">
                            <div class="col-md-3 table_in box_detail" data-aos="fade-up" data-aos-delay="800">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/004-gear.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">BANKRUPT 破產</h3>
                                        <p>當欠債人不能償還其債務，又或因失業 , 收入低 , 家庭生活開支等等原因...未能處理欠債，便可選擇申請破產來終止 銀行/財務公司等 滋擾性追數行動。破產是一種對個人及家庭生活的保障。這個保障是透過法律程序，保障欠債人不會因欠債導致個人及家庭成員的生活受還債壓力而有所影響。</p>
                                        <p><a href="bankrupt">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                            <div class="col-md-3 table_in box_detail" data-aos="fade-up" data-aos-delay="600">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/001-consultation.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">IVA 債務重組</h3>
                                        <p>債務重組（IVA）又稱為個人自願安排（Individual Voluntary Arrangement)）。申請IVA的最大原因，是欠債人士無法依照原有債務規定還債，因此希望債主體恤自己情況，跟債主磋商一個新的還款方案。</p>
                                        <p><a href="iva">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                            <div class="col-md-3 table_in box_detail" data-aos="fade-up" data-aos-delay="700">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/002-discussion.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">DRP 債務舒緩</h3>
                                        <p>債務舒緩（DRP），是IVA的簡化版，毋須經法庭申請重整債務。欠債人可自行跟債主商量，達成新的還款方案，包括降低利息或重訂還款年期；也可以尋找律師擬定還款建議書，詳列欠債人的資產及負債狀況，跟債主磋商。</p>
                                        <p><a href="drp">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                            <div class="col-md-3 table_in box_detail" data-aos="fade-up" data-aos-delay="800">
                                <div class="media block-icon-1 d-block text-left">
                                    <div class="icon mb-3">
                                        <img src="images/flaticon/svg/003-turnover.svg" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="media-body">
                                        <h3 class="h5 mb-4">IDRP 綜合債務舒緩</h3>
                                        <p>透過與欠債人的最大債權人商談還款方案的服務，只要最大債權人同意新提出的還款方案，欠債人所有的債務將會被統一歸納至最大債權人之下，並且按照新的還款建議書的還款方法，以定息定額之形式每月償還債款予最大債權人。</p>
                                        <p><a href="idrp">Learn More</a></p>
                                    </div>
                                </div> <!-- .block-icon-1 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <div class="templateux-section bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 testimonial-wrap">
                        <div class="quote">&ldquo;</div>
                        <div class="owl-carousel wide-slider-testimonial">
                            <div class="item">
                                <blockquote class="block-testomonial">
                                    <p>債務重組 （IVA）、債務舒緩（DRP），以及綜合債務舒緩（IDRP），都是為欠債人士而設的解決方案，不過3種解決方案當中，卻又有很大差別。有的是在金額上的分別，有的需要驚動法庭，有的方案則不容許申請人持有信用卡，亦需將每個月的一半薪金，用作還款。</cite></p>
                                </blockquote>
                            </div>
                            <div class="item">
                                <blockquote class="block-testomonial">
                                    <p>很多負債人仕都會擔心，申請債務舒緩的手續費非常昂貴，更擔心即時要繳付費用，本公司收費非常公平，透明度高，以客戶所慳利息的百份比來計算收費，而且申請時不用即時繳付費用，費用亦可以在客戶有能力的情況下，分期付款，協助客戶渡過負債沈重的難關。</cite></p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- .container -->
        </div> <!-- .templateux-section -->
        <div class="templateux-section">
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="col-lg-7">
                        <h2 class="mb-5">樓按加按</h2>
                        <div class="owl-carousel wide-slider">
                            <div class="item">
                                <img src="images/hm_1.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="images/hm_3.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="images/hm_2.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
                            </div>
                        </div> <!-- .owl-carousel -->
                    </div>
                    <div class="col-lg-5 pl-lg-5">
                        <h2 class="mb-5">Why Us?</h2>
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="mb-0 rounded mb-2">
                                    <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        加按/轉按/二按清債計劃</a>
                                </h2>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>適合擁有正資產物業的人士 (半邊業權也可)</p>
                                        <p>最高可借85%樓價</p>
                                        <p>非統一樓按及其他債務, 定額還款, 以減低利息開支</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="mb-0 rounded mb-2">
                                    <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        加按/轉按/二按清債申請程序
                                    </a>
                                </h2>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>提供現時物業資料、樓按餘額、還款紀錄等</p>
                                        <p>提供近三個月入息記錄</p>
                                        <p>提供私人貸款 / 信用咭等欠債資料</p>
                                        <p>本公司會為客戶進行估價及選取最優惠最合適的按揭計劃</p>
                                        <p>本公司安排客戶到有關銀行或財務機構簽署有關之按揭文件</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="mb-0 rounded mb-2">
                                    <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        加按/轉按清債參考個案
                                    </a>
                                </h2>
                                <style>
                                </style>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <table class="money_table" style="border: solid 1px;">
                                            <tr>
                                                <td></td>
                                                <td>卡數/貸款</td>
                                                <td>樓按</td>
                                                <td>加按清卡數</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>結餘</td>
                                                <td>HK$ 500,000</td>
                                                <td>HK$ 1,200,000</td>
                                                <td>HK$ 1,700,000</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>每月還款</td>
                                                <td>HK$25,000</td>
                                                <td>HK$ 7,800</td>
                                                <td>HK$ 13,400</td>
                                                <td>還款額大減60%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <div class="templateux-section bg-light py-5" id="templateux-counter-section">
            <div class="container">
                <div class="row">
                    <div class="col-md">
                        <div class="templateux-counter text-center">
                            <span class="templateux-number" data-number="2876">0</span>
                            <span class="templateux-label">客戶</span>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="templateux-counter text-center">
                            <span class="templateux-number" data-number="5935">0</span>
                            <span class="templateux-label">網上查詢</span>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="templateux-counter text-center">
                            <span class="templateux-number" data-number="3259">0</span>
                            <span class="templateux-label">電話查詢</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- .templateux-section -->
    <div class="templateux-section pb-0">
        <div class="container">
            <div class="row text-center mb-5">
                <div class="col-12">
                    <h2>參考案例</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-sm-6 col-md-6 col-lg-4">
                    <div class="staff" class="staff-img" style="background-image: url(images/c8.jpg);">
                        <a href="case?1" class="desc">
                            <h3>姓別：男</h3>
                            <span>職業：建築工人　月入：$22,000</span>
                            <div class="parag">
                                <p>查看案例</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-4">
                    <div class="staff" class="staff-img" style="background-image: url(images/p2.jpg);">
                        <a href="case?2" class="desc">
                            <h3>姓別：女</h3>
                            <span>職業：保險從業員(7年)　月入：$30,000</span>
                            <div class="parag">
                                <p>查看案例</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-4">
                    <div class="staff" class="staff-img" style="background-image: url(images/c9.jpg);">
                        <a href="case?3" class="desc">
                            <h3>姓別：女</h3>
                            <span>職業：銀行管理層(19年)　月入：$75,000</span>
                            <div class="parag">
                                <p>查看案例</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="templateux-section bg-light">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center mb-5">
                    <h2>其他案例</h2>
                </div>
                <div class="col-md-4 mb-4" data-aos="fade-up">
                    <a href="case4">
                        <div class="media block-icon-1 d-block text-center">
                            <div class="icon mb-3">
                                <img src="images/flaticon/svg/004-gear.svg" alt="Image" class="img-fluid">
                            </div>
                            <div class="media-body">
                                <h3 class="h5 mb-4">姓別：女</h3>
                                <p>職業：鋼琴私人老師　月入：$10,000-$11,000</p>
                            </div>
                        </div>
                    </a> <!-- .block-icon-1 -->
                </div>
                <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="100">
                    <a href="case5">
                        <div class="media block-icon-1 d-block text-center">
                            <div class="icon mb-3">
                                <img src="images/flaticon/svg/006-meeting.svg" alt="Image" class="img-fluid">
                            </div>
                            <div class="media-body">
                                <h3 class="h5 mb-4">姓別：男</h3>
                                <p>職業：自顧人士　月入：$8,000~22,000</p>
                            </div>
                        </div> <!-- .block-icon-1 -->
                    </a> <!-- .block-icon-1 -->
                </div>
                <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="200">
                    <a href="case6">
                        <div class="media block-icon-1 d-block text-center">
                            <div class="icon mb-3">
                                <img src="images/flaticon/svg/004-gear.svg" alt="Image" class="img-fluid">
                            </div>
                            <div class="media-body">
                                <h3 class="h5 mb-4">姓別：男</h3>
                                <p>職業：的士司機　月入：$17,000</p>
                            </div>
                        </div> <!-- .block-icon-1 -->
                    </a> <!-- .block-icon-1 -->
                </div>
                <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="300">
                    <a href="case9">
                        <div class="media block-icon-1 d-block text-center">
                            <div class="icon mb-3">
                                <img src="images/flaticon/svg/006-meeting.svg" alt="Image" class="img-fluid">
                            </div>
                            <div class="media-body">
                                <h3 class="h5 mb-4">姓別：女</h3>
                                <p>職業：從事船務職業　月入：$18,000</p>
                            </div>
                        </div> <!-- .block-icon-1 -->
                    </a> <!-- .block-icon-1 -->
                </div>
                <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="400">
                    <a href="case7">
                        <div class="media block-icon-1 d-block text-center">
                            <div class="icon mb-3">
                                <img src="images/flaticon/svg/004-gear.svg" alt="Image" class="img-fluid">
                            </div>
                            <div class="media-body">
                                <h3 class="h5 mb-4">姓別：男</h3>
                                <p>職業：經營生意</p>
                            </div>
                        </div> <!-- .block-icon-1 -->
                    </a> <!-- .block-icon-1 -->
                </div>
                <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="500">
                    <a href="case8">
                        <div class="media block-icon-1 d-block text-center">
                            <div class="icon mb-3">
                                <img src="images/flaticon/svg/006-meeting.svg" alt="Image" class="img-fluid">
                            </div>
                            <div class="media-body">
                                <h3 class="h5 mb-4">姓別：男</h3>
                                <p>職業：文員　月入：$26,000</p>
                            </div>
                    </a> <!-- .block-icon-1 -->
                </div> <!-- .block-icon-1 -->
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-12 text-center" data-aos="fade-up" data-aos-delay="100">
          <p><a href="#" class="btn btn-black py-3 px-4">More Features</a></p>
        </div> -->
        </div>
    </div>
    </div> <!-- .templateux-section -->
    <div class="container templateux-section" style="overflow: hidden;">
        <div class="row">
            <div class="col-12 col-md-7 mx-auto text-center mb-5">
                <h2>客戶的見證</h2>
                <p>真心出發，以你為本 各種方案配合不同需要。</p>
            </div>
        </div>
        <div class="d-flex flex-sm-row">
            <a class="block-thumbnail-1 one-third" style="background-image: url('images/g3.jpg'); " data-aos="fade">
                <div class="block-thumbnail-content">
                    <h2>姓別：男(35歲)　職業：採購部經理　月入：$35,000</h2>
                    <span class="post-meta">
                        供款不准時，短期內申請信用卡、貸款、樓按次數過多都會影響信貸評級。信貸不良就會導致申請得不到批核。過往曾遇過好多因為破過產，習慣性遲還款的客戶，因為信貸積分過低，有樓做抵押，都得不到銀行批核，我司已經為無數人成功辦理樓宇按揭，對銀行審核需要甚麼文件、審核標準都非常熟悉。</span>
                </div>
            </a>
            <a class="block-thumbnail-1 two-third" style="background-image: url('images/b6.jpg'); " data-aos="fade" data-aos-delay="100">
                <div class="block-thumbnail-content">
                    <h2>姓別：男(40歲)　職業：建築工人　月入：$22,000</h2>
                    <span class="post-meta">許多人以為有磚頭在手，銀行一定就會批貸款。其實不然，還款能力、信貸情況，銀行十分看重。
                    </span>
                    <span class="post-meta">我司在為客戶進行物業重按的時候經常遇到，所有資料已準備完整，銀行方面也已經初步過關，但卻遲遲不能批核，原因就是還欠一份入息證明！也試過，客戶的公司信只是打錯了一個標點符號，銀行便需要他更改重新呈交！許多人在進行物業按揭時，會因為入息證明這關卡住，最後放棄整個申請。可想而知一份入息證明如何重要！本公司根據多年的按揭經驗，即使入息有問題，我司亦可幫閣下成功進行按揭申請。
                    </span>
                </div>
            </a>
        </div>
        <div class="d-flex flex-column flex-sm-row">
            <a class="block-thumbnail-1 two-third" style="background-image: url('images/c3.jpg'); " data-aos="fade" data-aos-delay="200">
                <div class="block-thumbnail-content">
                    <h2>姓別：男(42歲)　職業：註册護士　月入：$49,000</h2>
                    <span class="post-meta">協助客戶加按，轉按的時候，最怕遇到的問題便是銀行估價不足，無法清還外債，產生壓力測試問題。然而一個有豐富經驗的顧問，便知道這一個物業，即使銀行一開始的估價不足，也是可以申請，可以協商的。這是經驗累積而來的知識，箇中因由，並不是三言兩句便能道出！所以找一個有經驗的公司是多麼的重要！即使困難，也可以為您找到適合的辦法！</span>
                </div>
            </a>
            <a class="block-thumbnail-1 one-third" style="background-image: url('images/g1.jpg'); " data-aos="fade" data-aos-delay="300">
                <div class="block-thumbnail-content">
                    <h2>姓別：女(28歲)　職業：註冊護士　月入：$30,000</h2>
                    <span class="post-meta">由於申請轉按的文件比較繁複，因此銀行不批核的原因很多，如果一開始呈交的文件不足，可能已經會導致申請不批核。另外再加上，每次申請被拒，信貸評分均會下降，這樣便大大地降低了去其他銀行批核的機會。本公司在申請樓按擁有非常豐富的經驗，加上本公司與本港多間銀行長期合作。經顧問的分析後，本公司可以為您找到最適合您的銀行做貸款，務求一擊即中，省卻您煩心和時間。</span>
                </div>
            </a>
        </div>
    </div> <!-- .templateux-section -->
    <?php include("contact_form.php"); ?>
    <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>

    <script src="js/scripts-all.js"></script>
        <script src="js/main.js"></script>
    <script src="js/home.js"></script>
</body>

</html>