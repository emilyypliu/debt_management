<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-image: url(images/hero_1.jpg);">
            <div class="container">
                <div class="row align-items-lg-center">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class="heading mb-3 text-white" data-aos="fade-up">有機會幫你大減60%還款額</h1>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <style>
            .debt_range_wrapper  {
              position: relative;
              width: 70%;
              margin: 0 15%;
            }
            
            .debt_range_wrapper input[type=range] {
              -webkit-appearance: none; 
              width: 100%; 
              background: transparent; 
            }
            
            .debt_range_wrapper input[type=range]::-webkit-slider-runnable-track {
              -webkit-appearance: none;
              height: 1.5rem;
              border-radius: 0.75rem;
              background: #b5d8ff;
            }

            .debt_range_wrapper input[type=range]::-moz-range-track {
              -webkit-appearance: none;
              height: 1.5rem;
              border-radius: 0.75rem;
              background: #b5d8ff;
            }
            
            .debt_range_wrapper input[type=range]::-moz-range-progress {
              background: #1e3650;
              height: 1.5rem;
              border-radius: 0.75rem 0 0 0.75rem;
            }
            
            .debt_range_wrapper input[type=range]::-ms-track {
              -webkit-appearance: none;
              height: 1.5rem;
              border-radius: 0.75rem;
              background: #b5d8ff;
              color: transparent;
              border: 0;
            }
            
            .debt_range_wrapper input[type=range]::-ms-fill-lower {
              background: #1e3650;
              border-radius: 0.75rem 0 0 0.75rem;
            }
            
            
            .debt_range_wrapper input[type=range]::-webkit-slider-thumb {
              -webkit-appearance: none;
              height: 1.5rem;
              width: 1.5rem;
              border-radius: .75rem;
              background: #ffffff;
              cursor: pointer;
              margin-top: 0;
            }
            
            .debt_range_wrapper input[type=range]::-moz-range-thumb {
              -webkit-appearance: none;
              height: 1.5rem;
              width: 1.5rem;
              border-radius: 0.75rem;
              background: #ffffff;
              cursor: pointer;
              margin-top: -14px; 
            }
            
            .debt_range_wrapper input[type=range]::-ms-thumb {
              height: 1.5rem;
              width: 1.5rem;
              border-radius: 0.75rem;
              background: #ffffff;
              cursor: pointer;
              margin-top: -14px;
            }
            
            .debt_range_wrapper .visually-hidden { 
                position: absolute !important;
                height: 1px; 
                width: 1px;
                overflow: hidden;
                clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
                clip: rect(1px, 1px, 1px, 1px);
                white-space: nowrap; /* added line */
            }
        </style>
        <div class="container templateux-section">
            <div class="row">
                <div class="col-12 col-md-7 mx-auto text-center mb-5">
                    <h2>慳息計算機</h2>
                    <p>有機會幫你大減60%還款額</p>
                </div>
            </div>
            <div class="row" style="padding: 10px; background-color: #f3f3f3;">
                <!-- <div class="mainContainer">       -->
                <div class="messageError"></div>
                <form class="row" style="width: 100%; text-align: center;">
                    <div class="col-md-6 mb-6" data-aos="fade-up" data-aos-delay="300">
                        <p>銀行 總共欠款</p>
                        <input type="text" class="bank_debt" name="bank_debt" value="100000">
                        <div class="debt_range_wrapper">
                            <span style="float:left">要求還款期數</span> <span style="float: right" id="demo"></span>
                            <input id="progress" type="range" min="12" max="120" step="6" data-before />
                            <label class="visually-hidden" for="progress">Played</label>
                        </div>
                    </div>
                    <!-- <span class="operator">+</span> -->
                    <div class="col-md-6 mb-6" data-aos="fade-up" data-aos-delay="300">
                        <p>財務公司 總共欠款</p>
                        <input type="text" class="company_debt" name="company_debt" value="100000">
                        <div class="debt_range_wrapper">
                            <span style="float:left">要求還款期數</span> <span style="float: right" id="demo2"></span>
                            <input id="progress2" type="range" min="12" max="120" step="6" data-before />
                            <label class="visually-hidden" for="progress">Played</label>
                        </div>
                    </div>
                </form>
                </br>
            </div>
            <div class="calc-subtotal calc-list row loaded" style=" padding: 44px; background-color: rgb(244, 247, 248); margin-top: 14px;">
                <div class="calc-item title" style="padding: 10px; ">
                    <h4 style="color: rgb(0, 0, 0); font-size: 22px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 0px;">詳情</h4>
                </div>
                <div class="calc-subtotal-list" style="width: 100%;">
                    <div class="sub-list-item quantity_field_id_0" style="color: rgb(0, 0, 0); font-style: normal; font-size: 16px; text-align: left; font-weight: 400; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 0px;">
                        <div>
                            <div><span class="sub-item-title"> 銀行 總共欠款</span> $<span class="bank_debt_num"> 100,000 </span></div>
                            <!---->
                            <div class="sub-list-item "><span class="sub-item-title"> 要求還款期數</span> <span class="sub-item-value"> 30 </span></div>
                            <!---->
                            <div class="sub-list-item "><span class="sub-item-title"> 財務公司 總共欠款</span> $<span class="company_debt_num"> 100,000 </span></div>
                            <!---->
                            <div class="sub-list-item "><span class="sub-item-title">要求還款期數</span> <span class="sub-item-value"> 30 </span></div>
                            <!---->
                        </div>
                        <div style="float: right;">
                            <div class="sub-list-item total"><span class="sub-item-title" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;"> 估計銀行每月供款 </span> <span id="bank_de" class="sub-item-value bank_de" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;float: right;"> $ 3,583.33 </span></div>
                            <div class="sub-list-item total"><span class="sub-item-title" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;"> 估計財務公司每月供款 </span> <span id="company_de" class="sub-item-value" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;float: right;"> $ 4,333.33 </span></div>
                            <div class="sub-list-item total"><span class="sub-item-title" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;"> 現時欠款總數 </span> <span class="sub-item-value answer" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;float: right;"> $ 200,000.00 </span></div>
                            <div class="sub-list-item total"><span class="sub-item-title" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;"> 全期總利息 </span> <span id="interest" class="sub-item-value" style="color: rgb(0, 0, 0); font-size: 16px; font-style: normal; font-weight: 700; text-shadow: rgba(255, 255, 255, 0) 0px 0px 0px; letter-spacing: 4px;float: right;"> $ 37,500.00 </span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="templateux-section ">
                <div class="container">
                    <div class="row mb-5">
                        <div class="col-md-12 mb-4" data-aos="fade-up">
                            <h3>如何收費</h3>
                            <p>很多負債人仕都會擔心，申請債務舒緩的手續費非常昂貴，更擔心即時要繳付費用，本公司收費非常公平，透明度高，以客戶所慳利息的百份比來計算收費，而且申請時不用即時繳付費用，費用亦可以在客戶有能力的情況下，分期付款，協助客戶渡過負債沈重的難關。</p>
                        </div>
                        <a href="index.html#contact" class="btn btn-primary py-3 px-4 mr-3">立即聯絡我們</a>
                    </div>
                </div>
            </div> <!-- .templateux-section -->
            <div class="templateux-section bg-light">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 testimonial-wrap">
                            <div class="quote">&ldquo;</div>
                            <div class="owl-carousel wide-slider-testimonial">
                                <div class="item">
                                    <blockquote class="block-testomonial">
                                        <p>債務重組 （IVA）、債務舒緩（DRP），以及綜合債務舒緩（IDRP），都是為欠債人士而設的解決方案，不過3種解決方案當中，卻又有很大差別。有的是在金額上的分別，有的需要驚動法庭，有的方案則不容許申請人持有信用卡，亦需將每個月的一半薪金，用作還款。</cite></p>
                                    </blockquote>
                                </div>
                                <div class="item">
                                    <blockquote class="block-testomonial">
                                        <p>很多負債人仕都會擔心，申請債務舒緩的手續費非常昂貴，更擔心即時要繳付費用，本公司收費非常公平，透明度高，以客戶所慳利息的百份比來計算收費，而且申請時不用即時繳付費用，費用亦可以在客戶有能力的情況下，分期付款，協助客戶渡過負債沈重的難關。</cite></p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container -->
            </div> <!-- .templateux-section -->
        </div> <!-- .js-animsition -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
    <script src="js/price.js"></script>
</body>

</html>