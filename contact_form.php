<?php
?>

<div class="templateux-section " id="contact">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7 mx-auto text-center mb-5">
                <h2>聯絡我們</h2>
                <p>為您立即提供協助及詳盡分析</p>
            </div>
        </div>
<?php if(isset($contact_form)): ?>
        <div class="row">
            <div class="alert <?php echo $contact_form['success'] ? 'alert-success' : 'alert-danger'; ?>">
                <?php echo $contact_form['alert_message'] ?? '郵件發出失敗， 請使用用其他方法連續我們'; ?>
            </div>
        </div>
<?php endif; ?>
        <div class="row mb-5">
            <div class="col-md-7 pr-md-7 mb-5">
                <form action="mail" method="post">
                    <div class="form-group">
                        <label for="name">名字</label>
                        <input name="name" type="text" class="form-control" id="name" value="<?php echo $contact_form['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">電郵</label>
                        <input name="email" type="email" class="form-control" id="email" value="<?php echo $contact_form['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="phone">電話號碼</label>
                        <input name="phone" type="phone" class="form-control" id="phone" value="<?php echo $contact_form['phone']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="message">查諮內容</label>
                        <textarea name="message" id="message" cols="30" rows="10" class="form-control"><?php echo $contact_form['message']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary py-3 px-5" value="發送">
                    </div>
                </form>
            </div>
            <div class="col-md-5">
                <!-- <div id="map"></div> -->
                <div class="media block-icon-1 d-block text-center">
                    <div class="icon mb-3"><span class="ion-ios-location-outline"></span></div>
                    <div class="media-body">
                        <h3 class="h5 mb-4">北角英皇道338號華懋二期607室</h3>
                    </div>
                </div> <!-- .block-icon-1 -->
                <div class="media block-icon-1 d-block text-center">
                    <div class="icon mb-3"><span class="ion-ios-telephone-outline"></span></div>
                    <div class="media-body">
                        <h3 class="h5 mb-4">+852 5173 1101</h3>
                    </div>
                </div> <!-- .block-icon-1 -->
                <div class="media block-icon-1 d-block text-center">
                    <div class="icon mb-3"><span class="ion-ios-email-outline"></span></div>
                    <div class="media-body">
                        <h3 class="h5 mb-4">chilokcompanylimited@gmail.com</h3>
                    </div>
                </div> <!-- .block-icon-1 -->
            </div>
        </div> <!-- .row -->
    </div>
</div>
