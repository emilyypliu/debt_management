<?php
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>香港債務重組服務社 | HONG KONG PROFESSIONAL DEBT RELIES SERVICE COMPANY</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>
    <style>
    .templateux-overlap .block-icon-1 {

        margin-top: 0px;
        position: relative;
    }

    .templateux-cover,
    .templateux-cover .row {
        min-height: 240px;
    }

    @media only screen and (max-width: 767px) {
        .box_detail {
            margin-bottom: 10px !important;
        }
    }
    </style>
    <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
        <?php include("header.html"); ?>
        <div class="templateux-cover" style="background-color: rgb(182, 180, 180); height: 200px; min-height: 200px;">
            <div class="container">
                <div class="row align-items-lg-center d">
                    <div class="col-lg-6 order-lg-1 text-center mx-auto">
                        <h1 class=" mb-3 text-white" data-aos="fade-up">Terms of Service</h1>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-cover -->
        <div class="templateux-section pt-0 pb-0">
            <div class="container">
                <div class="row">
                    <div class="c-copy u-text-hyphen-auto">
                        <p><b>WHO 'WE' ARE</b></p>
                        <p>Euler Hermes, 1 place des Saiso Euler Hermes is incorporated in Belgium with a registered address at Avenue des Arts 56, 1000 Brussels, Belgium and a company no. 0403.248.596 RPM Brussels.</p>
                        <p><br>
                            Euler Hermes is also registered as a non-Hong Kong company with a Hong Kong Companies Registry no. F0020088, and an address situated at Suites 403-11, 4/F, 12 Taikoo Wan Road, Taikoo Shing, Hong Kong.</p>
                        <p><br>
                            Euler Hermes is authorised by the National Bank of Belgium and the Belgian Financial Services and Markets Authority. Euler Hermes as a branch in Hong Kong is regulated by the Hong Kong Insurance Authority in accordance with the Insurance Ordinance of Hong Kong.</p>
                        <p><br>
                            Euler Hermes Hong Kong Services Limited is a company incorporated in Hong Kong with a Hong Kong Companies Registry no. 0656483 and a registered office address at Suites 403-11, 4/F, 12 Taikoo Wan Road, Taikoo Shing, Hong Kong.</p>
                        <p>&nbsp;</p>
                        <p><b>TERMS&nbsp; AND CONDITIONS OF USE</b></p>
                        <p>You should read these terms and conditions carefully before using this web site (the 'Site'). By accessing and using the Site you agree to be bound by the terms and conditions set out below. If you do not wish to be bound by these terms and conditions, you may not access or use the Site.</p>
                        <p><b><b><br>
                                </b>SITE CONTENT</b></p>
                        <p>Although we have taken all reasonable care to ensure that the information posted on the Site is current, true and accurate in all material respects and that there are no other material facts the omission of which would make any of the information misleading, we give no warranty, express or implied, with regard to the accuracy or currency of the information posted on the Site. The content of the Site is general in nature and for information purposes only.</p>
                        <p>&nbsp;</p>
                        <p><b>SITE ACCESS</b></p>
                        <p>We reserve the right to suspend and withdraw access to some or all of the pages of the Site without notice at any time and accepts no responsibility for these pages not being available. We do not warrant (1) the full functionality of the Site, (2) that defects in the Site will be corrected or (3) that the Site or the server that makes it available is free of viruses or bugs.&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><b>LINKS</b></p>
                        <p>Where we provide hypertext links to other web sites or contacts, we do so for information only and such links do not constitute an endorsement or recommendation by us of any products or services in such sites. You use such links entirely at your own risk and we accept no responsibility for the content or use of such web sites or for the information contained on such sites.&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><b>LICENCE</b></p>
                        <p>Unless otherwise stated all rights, including copyright, database rights and other intellectual property rights, in the Site and its contents are owned by (or licensed to) us and you are responsible for obeying all applicable copyright laws. You may use the information and take copies of the Site as necessary incidental acts during your viewing of it and you may take a print for your personal use of so much of the Site as is reasonable for private purposes. The information may not otherwise be reproduced, stored in any medium (including in any other web site), distributed or transmitted to any other person or incorporated in any way into another document or other material without the prior written permission of us.&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><b>USE OF THE SITE&nbsp;</b></p>
                        <p>You agree to use the Site only for lawful purposes and in a manner, which does not infringe the rights of, or restrict or inhibit the use and enjoyment of the Site by any third party. You agree not to import or transfer to the Site either directly or indirectly any information or other content which is or may be inaccurate, misleading, defamatory, obscene or offensive or in breach of any intellectual property right or similar right or damaging to the software or performance of the Site.&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><b>TERRITORY&nbsp;</b></p>
                        <p>The services described in the Site are only available to companies and/or residents to the extent that we are licensed to sell insurance within that territory. You agree to use the Site in a manner consistent with any and all applicable law and regulation in the country in which you access the Site.&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><b>SCOPE OF YOUR RESPONSIBILITY</b></p>
                        <p>You are wholly responsible for the use of the Site by any person using your terminal or point of presence and are responsible for ensuring that any such person also fully complies with these terms and conditions.&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><b>YOUR BREACH</b></p>
                        <p>If you are in breach of these terms and conditions you agree to indemnify us in respect of any and all losses, costs and/or damages including reasonable legal fees incurred by us in relation to and/or arising out of such breach.</p>
                        <p>&nbsp;</p>
                        <p><b>COOKIES</b>&nbsp;&nbsp;</p>
                        <p>We use cookies on this Site. This enables us to give you the best experience possible when using our Site. By continuing to use our Site, you agree to the use of cookies as explained in our cookies policy. If you would like to change your settings relating to cookies, please view this page on how to manage cookies.</p>
                        <p>&nbsp;</p>
                        <p><b>EXCLUSIONS&nbsp;&nbsp;</b></p>
                        <p>We make no warranties except as set out in these terms and conditions and to the extent permitted by law, all other express and implied warranties are hereby excluded.</p>
                        <p>&nbsp;</p>
                        <p><b>LIMITATION OF LIABILITY</b></p>
                        <p>To the extent permitted by law, we do not accept any responsibility for any losses and/or damages whatsoever including, without limitation, direct, indirect, special or consequential loss or damage and/or loss of profits, business revenue, goodwill or anticipated savings arising out of access to or use of or reliance on any information posted on the Site or any hypertext or hyperlink to a web site or any information contained on or accessed through such web site.</p>
                        <p>&nbsp;</p>
                        <p><b>ENTIRE AGREEMENT&nbsp;</b></p>
                        <p>These terms and conditions contain the entire understanding between you and us with regard to your use of the Site and no representation, statement or inducement, whether oral or written, not contained herein shall bind either you or us. We reserve the right to change these terms and conditions from time to time. Use of the Site is subject to the terms and conditions posted on the Site at the time of your visit. If you do not agree to any changes to the terms and conditions then you must immediately stop using the Site.</p>
                        <p>&nbsp;</p>
                        <p><b>NO INSURANCE CONTRACT&nbsp;</b></p>
                        <p>The information contained in the Site does not constitute an intention for us to enter into an insurance relationship with you nor be construed as an offer or solicitation for an offer or advice on any insurance or other related matters.&nbsp; Any contract of insurance between you and us will be governed by separate terms and conditions agreed between you and us.</p>
                        <p>&nbsp;</p>
                        <p><b>SEVERABILITY&nbsp;</b></p>
                        <p>Should any part of these terms and conditions for any reason be declared invalid by a court of competent jurisdiction, such determination shall not affect the validity of any remaining portion and such remaining portion shall remain in full force and effect as if the invalid portion of these terms and conditions had been eliminated.</p>
                        <p>&nbsp;</p>
                        <p><b>GOVERNING LAW&nbsp;&nbsp;</b></p>
                        <p>These terms and conditions and your use of the Site are governed by&nbsp;French law. Disputes arising out of these terms and conditions shall be subject to the exclusive jurisdiction of the Courts of Paris.<br>
                        </p>
                    </div>
                </div>
            </div>
        </div> <!-- .templateux-section -->
        <?php include("footer.html"); ?>
    </div> <!-- .js-animsition -->
    <script src="js/extras/jquery.min.js"></script>
    <script src="js/scripts-all.js"></script>
    <script src="js/main.js"></script>
</body>

</html>